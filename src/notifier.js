const WindowsBalloon = require('node-notifier').WindowsBalloon;
const notifier = new WindowsBalloon({
    withFallback: false, // Try Windows Toast and Growl first?
    customPath: undefined // Relative/Absolute path if you want to use your fork of notifu
});

exports.errNotify = (props) => {
    notifier.notify(
        {
            title: "Failed",
            sound: true,
            time: 5000, // How long to show balloon in ms
            wait: false, // Wait for User Action against Notification
            type: 'error', // The notification type : info | warn | error
            ...props,
        }
    )
}

exports.successNotify = (props) => {
    notifier.notify(
        {
            title: "Success",
            sound: true,
            time: 5000, // How long to show balloon in ms
            wait: false, // Wait for User Action against Notification
            type: 'info', // The notification type : info | warn | error
            ...props,
        }
    )
}
