### Dashboard server

An express server that handles the API backend and serves the frontend as static files

## Using

Run `dashboard-server.exe` **as Administrator**  
Then open `localhost:4000`

## Issues
Project Build/Debug problems
- Make sure MSBuild is setup and is the correct version
Greasemonkey problem
- Make sure the .env is populated with the correct DB credentials

## Building 
First, make sure that dashboard-web is in the correct place: `../dashboard-web/`   
Run in CMD:
- `yarn`
- `yarn build`

## Development
The project is setup to be built on CMD. It'll probably work with other environment as well but will require some manual commands

```
Note: For frontend to work, we currently expect the folder to be one below the current directory. Eg: `../dashboard-web/`
```