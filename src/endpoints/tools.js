module.exports = function (app) {
    app.post('/api/tools/page-property/*', (req, res, next) => {
        if (!req.body.dbData) {
            res.status(400).json({ success: false, error: 'Database data not provided' })
        }

        const { dbServer, dbName, dbUsername, dbPassword } = req.body.dbData

        if (!dbServer || !dbName || !dbUsername || !dbPassword) {
            res.status(400).json({ success: false, error: 'Invalid Database data' })
        }

        res.locals.knex = require('knex')({
            client: 'mssql',
            connection: {
                host: dbServer,
                database: dbName,
                user: dbUsername,
                password: dbPassword,
            }
        });

        next()
    })

    app.post('/api/tools/page-property/evaluate', async (req, res) => {
        if (!req.body.query) {
            res.status(400).json({ success: false, error: 'Invalid Query' })
        }

        const query = req.body.query
        const knex = res.locals.knex

        try {
            const result = await knex.raw(query)
            res.json(result).status(200)
        } catch (e) {
            console.error(e)
            res.status(400).json({ success: false, error: e })
        }
    })

    app.post('/api/tools/page-property/preview', async (req, res) => {
        if (!req.body.query) {
            res.status(400).json({ success: false, error: 'Invalid Query' })
        }
        if (!req.body.code) {
            res.status(400).json({ success: false, error: 'Invalid Code' })
        }

        const query = req.body.query
        const code = req.body.code
        const knex = res.locals.knex

        try {
            const result = await knex.raw(query)

            const arrayToUpdate = result.map(row => {
                const contentPageParameters = JSON.parse(row.ContentPageParameters)
                const resultContentPageParameters = eval(`
                (
                    function(val) { ${code} } ( ${JSON.stringify(contentPageParameters)} )
                )`)

                return { ...row, ContentPageParameters: resultContentPageParameters }
            })

            res.json(arrayToUpdate).status(200)
        } catch (e) {
            console.error(e)
            res.status(400).json({ success: false, error: e })
        }
    })

    app.post('/api/tools/page-property/run', async (req, res) => {
        if (!req.body.query) {
            res.status(400).json({ success: false, error: 'Invalid Query' })
        }
        if (!req.body.code) {
            res.status(400).json({ success: false, error: 'Invalid Code' })
        }

        const query = req.body.query
        const code = req.body.code
        const knex = res.locals.knex

        try {
            const result = await knex.raw(query)

            const arrayToUpdate = result.map(row => {
                const contentPageParameters = JSON.parse(row.ContentPageParameters)
                const resultContentPageParameters = eval(`
                (
                    function(val) { ${code} } ( ${JSON.stringify(contentPageParameters)} )
                )`)

                return { ...row, ContentPageParameters: resultContentPageParameters }
            })

            const promises = arrayToUpdate.map(row => knex('tPageDefinition').update({ ContentPageParameters: JSON.stringify(row.ContentPageParameters) }).where(Object.fromEntries(Object.entries(row).filter(a => a[0] !== 'ContentPageParameters'))))

            await Promise.all(promises)

            res.json({ success: true }).status(200)
        } catch (e) {
            console.error(e)
            res.status(400).json({ success: false, error: e })
        }
    })

}

