const fs = require('fs')
const parser = require('fast-xml-parser')
const branch = require('git-branch')

if (!Array.prototype.last) {
  Array.prototype.last = function () {
    return this[this.length - 1]
  }
}

module.exports = function (app) {
  app.get('/api/main', (req, res) => {
    const projectFolderName = 'C:/VisualStudioProjects'
    const dllRegex = /(.+)\.(.+)/g
    const releaseDllDirectory = 'C:/Program Files (x86)/Parsec/TrakSYS/ts/bin'

    const releaseDir = fs.readdirSync(releaseDllDirectory)

    // Find all project
    const allProjectNames = fs
      .readdirSync(projectFolderName, { withFileTypes: true })
      .filter(dirent => dirent.isDirectory())
      .map(dirent => dirent.name)

    // Find all dll for each projects
    const releasedDll = allProjectNames.map(projectName => {
      const folders = fs
        .readdirSync(`${projectFolderName}/${projectName}`, { withFileTypes: true })
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name)
      const potentialDll = folders.filter(file => file.match(dllRegex))

      const releasedDll = potentialDll.filter(value => releaseDir.includes(`${value}.dll`))

      return { projectName, releasedDll }
    })

    const allReleasedDll = releasedDll.reduce((acc, arr) => [...acc, ...arr.releasedDll], [])

    // Starting here we step by step populate with data we need
    // For all the dll, we get the dependencies for each one
    const dllData = releasedDll.map(async ({ projectName, releasedDll }) => {
      const dllDependency = releasedDll.map(dllName => {
        const csProjectFilePath = `${projectFolderName}/${projectName}/${dllName}/${dllName}.csproj`
        const xml = fs.readFileSync(csProjectFilePath, { encoding: 'utf8' })

        const tObj = parser.getTraversalObj(xml)
        const jsonObj = parser.convertToJson(tObj)
        if (jsonObj.Project.ItemGroup[0].Reference) {
          const dependencies = jsonObj.Project.ItemGroup[0].Reference
            .filter(ref => ref.HintPath)
            .map(ref => ref.HintPath)
            .map(ref => ref.split('\\').last())
          return { dllName, dependencies }
        } else {
          return { dllName, dependencies: [] }
        }
      })

      const filteredDllDependency = dllDependency.map(({ dllName, dependencies }) => {
        const filteredDependencies = dependencies.filter(dll =>
          allReleasedDll.map(name => `${name}.dll`).includes(dll),
        )
        return { dllName, dependencies: filteredDependencies }
      })

      const dllDataWithModifiedDate = await Promise.all(filteredDllDependency.map(async ({ dllName, dependencies }) => {
        const stats = fs.statSync(`${releaseDllDirectory}/${dllName}.dll`)
        const theBranch = await branch(`${projectFolderName}/${projectName}`)

        return { dllName, dependencies, modifyTime: stats.mtime, branch: theBranch }
      }))

      return { [projectName]: dllDataWithModifiedDate }
    })

    Promise.all(dllData).then((processedDllData) => {
      const data = processedDllData.reduce((acc, obj) => ({ ...acc, ...obj }), {})
      res.json(data).status(200)
    })
  })
}

