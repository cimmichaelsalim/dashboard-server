const fs = require('fs')
const Shell = require('node-powershell');

const { successNotify, errNotify } = require('../notifier')

const projectFolderName = 'C:/VisualStudioProjects'
const dllRegex = /(.+)\.(.+)/g
// Can probably somehow put this into the regex
const banWords = ['Database', 'Test']
const releaseDllDirectory = 'C:/Program Files (x86)/Parsec/TrakSYS/ts/bin'

module.exports = function (app) {
  app.post('/api/build/clean', (req, res) => {
    if (!req.body.project) {
      res.status(400).json({ success: false, error: 'Invalid project name' })
    }
    const project = req.body.project

    const projectPath = `${projectFolderName}/${project}/`
    const slnName = fs.readdirSync(projectPath).find(file => file.match(/\.sln$/))
    const slnPath = `${projectFolderName}/${project}/${slnName}`

    const ps = new Shell({
      executionPolicy: 'Bypass',
      noProfile: true
    });

    ps.addCommand(`msbuild.exe '${slnPath}' -target:Clean`);
    ps.invoke()
      .then(output => {
        successNotify({ message: `Cleaning of ${project} successfull` })
        res.json({ success: "true" }).status(200)
      })
      .catch(err => {
        errNotify({ message: `Cleaning of ${project} failed` })
        console.log(err);
        res.status(500)
      });

  })
  app.post('/api/build/:config(debug|release)', (req, res) => {
    if (!req.body.project) {
      res.status(400).json({ success: false, error: 'Invalid project name' })
    }
    const project = req.body.project

    const release = req.params.config === "release"

    const projectPath = `${projectFolderName}/${project}/`
    const slnName = fs.readdirSync(projectPath).find(file => file.match(/\.sln$/))
    const slnPath = `${projectFolderName}/${project}/${slnName}`

    const ps = new Shell({
      executionPolicy: 'Bypass',
      noProfile: true
    });

    ps.addCommand(`msbuild.exe '${slnPath}'${release ? " /p:Configuration=Release" : ""}`);
    ps.invoke()
      .then(output => {
        const errorRegex = /(\d+) Error\(s\)/
        const match = output.match(errorRegex)[1]
        const errorCount = parseInt(match, 10)

        if (errorCount > 0) {
          errNotify({ message: `Building of ${project} failed\r${errorCount} Error(s)` })
          res.status(500).json({ success: "false", error: `Build failed with ${errorCount} Error(s)` })
          return
        }

        const potentialDlls = fs.readdirSync(projectPath, { withFileTypes: true }).filter(dirent => dirent.isDirectory())
          .map(dirent => dirent.name).filter(file => file.match(dllRegex) && banWords.every(ban => file.indexOf(ban) === -1))
        potentialDlls.forEach(potentialDll => {
          const dllFolderLocation = `${projectPath}/${potentialDll}/bin/${release ? "Release" : "Debug"}`

          const dllFileLocation = `${dllFolderLocation}/${potentialDll}.dll`
          const pdbFileLocation = `${dllFolderLocation}/${potentialDll}.pdb`

          fs.copyFileSync(dllFileLocation, `${releaseDllDirectory}/${potentialDll}.dll`)
          fs.copyFileSync(pdbFileLocation, `${releaseDllDirectory}/${potentialDll}.pdb`)
        })

        successNotify({ message: `Building of ${project} successfull` })
        res.json({ success: "true" }).status(200)
      })
      .catch(err => {
        errNotify({ message: `Unknown error when building ${project}` })
        console.log(err);
        res.status(500)
      });

  })
}

