require('dotenv-extended').load({ silent: false, errorOnMissing: true, includeProcessEnv: true })

// Check for env
if (
	!process.env.GENEROUS_CORS.match(/^(true|false)$/i)
) {
	// If any is not filled, exit
	console.error('Error: 1 or more necessary environment variables are not set correctly.')
	process.exit(1)
}

const server = require('./src/server')

// Setup main functionality
require('./src/endpoints/main')(server)
require('./src/endpoints/build')(server)
require('./src/endpoints/etc')(server)
require('./src/endpoints/tools')(server)

// Server the frontend
const express = require('express')
const path = require('path')
server.use('/', express.static(path.join(__dirname, '../dashboard-web/build')))

// Start listening
const port = process.env.PORT || 3000
server.listen(port)
console.log('Server listening on port ' + port)