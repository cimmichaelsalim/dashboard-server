const Shell = require('node-powershell');
const fs = require('fs')

const { successNotify, errNotify } = require('../notifier')

module.exports = function (app) {
    app.get('/api/etc/iisreset', (req, res) => {
        const ps = new Shell({
            executionPolicy: 'Bypass',
            noProfile: true
        });

        ps.addCommand(`iisreset`);
        ps.invoke()
            .then(output => {
                const success = output.includes("Internet services successfully restarted")
                if (!success) {
                    errNotify({ message: `IIS reset failed` })
                    res.status(500).json({ success: "false", error: `Failed to reset. Output: ${output}` })
                    return
                }
                successNotify({ message: `IIS reset success` })
                res.json({ success: "true" }).status(200)
            })
            .catch(err => {
                errNotify({ message: `FATAL ERROR: Reset IIS` })
                console.log(err);
                res.status(500)
            });
    })

    app.get('/api/etc/open-traksys-folder', (req, res) => {
        require('child_process').exec('start "" "C:/Program Files (x86)/Parsec/TrakSYS/ts/bin"');
        res.json({ success: true }).status(200)
    })

    // Grease monkey stuff
    app.get('/api/etc/open-page-edit/', async (req, res) => {
        const fromUrl = req.query.from
        const splitBySlash = fromUrl.split('?')[0].split('/')
        const key = splitBySlash[splitBySlash.length - 1] === '' ?
            splitBySlash[splitBySlash.length - 2] :
            splitBySlash[splitBySlash.length - 1]

        const urlHost = fromUrl.split('/TS')[0].split("/").slice(-1)[0]
        const host = req.query.dbServer === "(local)" ? urlHost : req.query.dbServer
        const database = req.query.dbName

        const data = fs.readFileSync('C:/Program Files (x86)/Parsec/TrakSYS/traksys.config')
        const match = data.toString('utf-8').match(/server="(.+)" name="(.+?)"/)
        const databaseHost = match[1]

        const knex = require('knex')({
            client: 'mssql',
            connection: {
                host: host === 'localhost' ? databaseHost : host,
                database,
                user: process.env.DB_USERNAME,
                password: process.env.DB_PASSWORD,
            }
        });

        const result = await knex('tPageDefinition').select('ID').where({ Key: key })

        if (result.length === 0) {
            res.json({ success: false, error: 'Page not found' }).status(400)
            return
        }

        res.redirect(`${fromUrl.split('TS')[0]}TS/pages/${fromUrl.split('/pages/')[1].split('/')[0]}/dev/pagedef/?c=ETS.Configuration.Properties&EntityName=PageDefinition&ID=${result[0].ID}`)
    })
}

